#!/usr/bin/env bash

# Abort if anything fails
#set -e

echo "Please enter dev id (mysite - NEEDS TO MATCH FOLDER NAME): "
read input_url

echo "Please enter eventual domain extension (com): "
read input_ext

echo "Which server will it be on? (server.ca1.leevdesigns or creatria): "
read input_server

# Get the docksal settings files
#mkdir .docksal/settings_files
#cp docroot/sites/default/default.settings.local.php .docksal/settings_files/default.settings.local.php
#cp docroot/sites/default/settings.php .docksal/settings_files/settings.php

#rm -rf docroot
# rm -rf drush
rm -rf .git

echo "Installing drupal via drupal recommended project..."
docker run --rm -u $(id -u):$(id -g) -v $(pwd):/app composer create-project drupal/recommended-project:^10 drupal --ignore-platform-req=ext-gd --no-interaction
cd drupal
echo "LVH: Adding composer dependencies..."
docker run --rm -u $(id -u):$(id -g) -v $(pwd):/app composer require drush/drush --ignore-platform-req=ext-gd
docker run --rm -u $(id -u):$(id -g) -v $(pwd):/app composer config --no-plugins allow-plugins.cweagans/composer-patches true
docker run --rm -u $(id -u):$(id -g) -v $(pwd):/app composer require cweagans/composer-patches --ignore-platform-req=ext-gd
cd ..

cp -r drupal/* .
rm -rf drupal
#cp .docksal/settings/* web/sites/default/
mkdir web/themes/custom
echo "Custom themes go here." > web/themes/custom/README.txt


# setup drush aliases
sed -i -e "s/NEWSITE/$input_url/g" drush/sites/NEWSITE.site.yml
sed -i -e "s/SERVER/$input_server/g" drush/sites/NEWSITE.site.yml
mv drush/sites/NEWSITE.site.yml drush/sites/"$input_url".site.yml

#update readme and reinstall script
sed -i -e "s/NEWSITE/$input_url/g" README.md
sed -i -e "s/EXT/$input_ext/g" README.md
sed -i -e "s/SERVER/$input_server/g" README.md
sed -i -e "s/NEWSITE/$input_url/g" reinstall.sh

mkdir -p ./web/modules/custom

git init
git add .
git commit -m "initial commit" --quiet
#git checkout -b "develop"

fin init