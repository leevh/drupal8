# Lee's Drupal starterkit

1.  Clone this repo: `git clone git@gitlab.com:leevh/drupal.git NEWSITE`

2.  cd NEWSITE and run `./install.sh`  This will:
  -  Setup the new local site
  -  Run *fin init* for docksal
  -  Clean up folders and start a fresh git repo with an initial commit


## Remote setup for NEW Project

1. SSH into BOA user and git clone new codebase into ~/static, `live-NEWSITE` and `test-NEWSITE`
2. cd into directory and `composer install --no-dev`

3. Set permissions from /static: 
```
chmod 775 live-NEWSITE && chmod 775 live-NEWSITE/web && chmod 775 live-NEWSITE/web/modules && chmod 775 live-NEWSITE/web/modules && chmod -R 775 live-NEWSITE/web/sites && chown -R host:www-data live-NEWSITE &&
chmod 775 test-NEWSITE && chmod 775 test-NEWSITE/web && chmod 775 test-NEWSITE/web/modules && chmod 775 test-NEWSITE/web/modules && chmod -R 775 test-NEWSITE/web/sites && chown -R host:www-data test-NEWSITE
```
3. Add/verify new platforms.  Specify platform path is at `/web`.

4. Create new site on live platform called `live.NEWSITE.SERVER.com`


6. add settings to local.settings.php (AS ROOT IN SSH)
```
cd /data/disk/host/static/live-NEWSITE/web/sites/live.NEWSITE.SERVER.com && nano local.settings.php
```


```
 $config_directories['sync'] = '../config/default';

 # File system settings.
// Get the current domain from $_SERVER['HTTP_HOST']
$currentDomain = $_SERVER['HTTP_HOST'];

// Parse out just the host without protocol or path
$domainParts = parse_url('http://' . $currentDomain); // Add http:// for parsing
$domain = $domainParts['host'];
 $settings["file_temp_path"] = 'sites/'.$domain.'/private/temp';
```


7. (ON LIVE ONLY) Use onetime login to change admin username to lvd-admin and secure password

8. Clone live site to test.NEWSITE.SERVER.com on the TEST platform

## Sync up Databases

9. Pull down databases by CD into web, and make sure terminal size is normal
on local: 
  - `ssh o1.ftp@o1.server.SERVER.com 'drush @NEWSITE.EXT sql-dump' > db.sql && fin db import db.sql`

on server:
  - `drush sql-sync @live.NEWSITE.SERVER.com @test.NEWSITE.SERVER.com --create-db`

tips for o1.ftp user ssh:
 - ssh root, switch user to o1.ftp, add laptop public key to authorized_keys file.
 - reset o1.ftp password if don't have it
 - as root disable password auth in etc/ssh/sshd_config. `service ssh restart` 

10. Synchronize config with `drush cex` locally, push to git and pull down on test and live, then `drush cim`, `drush updb`, and `drush cr`

## File Sync

- `fin drush core-rsync @NEWSITE.live:%files/ @NEWSITE.dev:%files`
- `drush core-rsync --omit-dir-times @live.NEWSITE.SERVER.com:%files/ @test.NEWSITE.SERVER.com:%files`


## Local DB operations

- `fin drush sql-dump > ../backup-DATE.sql`


## GO LIVE

- In live site on boa, change local.settings.php to:

```
 $config_directories['sync'] = '../config/default';
 # File system settings.
 $config['system.file']['path']['temporary'] = 'sites/LIVEDOMAIN.COM/private/temp';
```

- Change URI to live domain in drush alias
- Change to live domain in this document for future copy and pasting

## Other Information

- `drush sa` to see aliases
- `to run update db on BOA, use drush @site-name updb`
- For full proper git access on platforms, login with root and `su -s /bin/bash - host`
- 403 permissions errors in docksal? try `find . -type d -exec chmod -R a+rx '{}' \;` in the project root
- problems with available updates page? try `delete from key_value where collection='update_fetch_task'`
- Mismatched entity and/or field definitions?  `drush entity-updates`

## Remote setup for EXISTING Projects

TODO